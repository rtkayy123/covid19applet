package com.rtkay.apps.data;


import com.rtkay.apps.FetchAPI;
import com.rtkay.apps.data.model.CountryData;
import com.rtkay.apps.data.model.CovidData;
import com.rtkay.apps.data.model.GlobalData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class DataProviderService {
    public CovidData getData(String countryName) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://coronavirus-19-api.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchAPI fetchAPI = retrofit.create(FetchAPI.class);

        CompletableFuture<GlobalData> globalCallback = new CompletableFuture<>(); //listens and waits for obj to have val
        CompletableFuture<CountryData> countryCallback = new CompletableFuture<>(); //listens and waits for obj to have val
        CompletableFuture<List<CountryData>> countryNameCallback = new CompletableFuture<>(); //listens and waits for obj to have val

      Call<GlobalData> globalDataCall = fetchAPI.getGlobalData();
       globalDataCall.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<GlobalData> call, Response<GlobalData> response) {
                System.out.println(response.body());
                globalCallback.complete(response.body());
            }

            @Override
            public void onFailure(Call<GlobalData> call, Throwable t) {
                globalCallback.completeExceptionally(t);
            }
        });

        Call<CountryData> loadSelectedCountry = fetchAPI.getCountryData(countryName);
        loadSelectedCountry.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<CountryData> call, Response<CountryData> response) {
                System.out.println(response.body());
                countryCallback.complete(response.body());
            }

            @Override
            public void onFailure(Call<CountryData> call, Throwable t) {
                countryCallback.completeExceptionally(t);
            }
        });

        Call<List<CountryData>> loadCountries = fetchAPI.getCountries();
        loadCountries.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<List<CountryData>> call, Response<List<CountryData>> response) {
                for (CountryData size : response.body()) {
                    System.out.println(size.toString());
                }
                countryNameCallback.complete(response.body());
            }

            @Override
            public void onFailure(Call<List<CountryData>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });

        GlobalData globalData = globalCallback.join();
        CountryData countryData = countryCallback.join();
        List<CountryData> countryNames = countryNameCallback.join();
        return new CovidData(globalData, countryData, countryNames);
    }
}
