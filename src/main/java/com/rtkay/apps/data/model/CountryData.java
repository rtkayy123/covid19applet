package com.rtkay.apps.data.model;

public record CountryData(String country,
                          long recovered,
                          long cases,
                          long critical,
                          long deathsPerOneMillion,
                          long active,
                          long deaths,
                          long testsPerOneMillion,
                          long todayCases,
                          long todayDeaths,
                          long totalTests) {
}
