package com.rtkay.apps.data.model;

public record GlobalData(long recovered, long cases, long deaths) {
}
