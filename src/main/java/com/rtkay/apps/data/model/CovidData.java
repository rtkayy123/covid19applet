package com.rtkay.apps.data.model;

import java.util.List;

public record CovidData(GlobalData globalData, CountryData countryData, List<CountryData>countryList) {
}
