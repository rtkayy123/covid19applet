package com.rtkay.apps;


import com.rtkay.apps.data.model.CountryData;
import com.rtkay.apps.data.model.GlobalData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface FetchAPI {
    @GET("https://coronavirus-19-api.herokuapp.com/all")
    Call<GlobalData> getGlobalData();

    @GET("https://coronavirus-19-api.herokuapp.com/countries/{countryName}")
    Call<CountryData> getCountryData(@Path(value = "countryName") String countryName);

    @GET("https://coronavirus-19-api.herokuapp.com/countries")
    Call<List<CountryData>> getCountries();
}
