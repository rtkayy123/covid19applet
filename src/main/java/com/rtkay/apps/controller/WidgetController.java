package com.rtkay.apps.controller;

import animatefx.animation.AnimationFX;
import animatefx.animation.RotateIn;
import com.jfoenix.controls.JFXDialog;
import com.rtkay.apps.config.Config;
import com.rtkay.apps.config.ConfigService;
import com.rtkay.apps.data.DataProviderService;
import com.rtkay.apps.data.model.CountryData;
import com.rtkay.apps.data.model.CovidData;
import com.rtkay.apps.data.model.GlobalData;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import org.kordamp.ikonli.javafx.FontIcon;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.rtkay.apps.Driver.cachingClassLoader;

public class WidgetController implements Initializable {
    @FXML
    private StackPane rootStackPane;
    @FXML
    private FontIcon btnRefresh;
    @FXML
    private Text txtGlobalReport;
    @FXML
    private Text txtCountryCode;
    @FXML
    private Text txtCountryReport;
    private ScheduledExecutorService executorService;
    private Config config;
    private AnimationFX syncAnimation;
    private List<CountryData> countryList;

    public void minimiseApp(MouseEvent event) {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        s.setIconified(true);
    }


    public void closeApp(MouseEvent event) {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        s.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        syncAnimation = new RotateIn(btnRefresh).setCycleCount(1000);
        try {
            syncAnimation.play();
            config = new ConfigService().getConfiguration();

        } catch (IOException e) {
            e.printStackTrace();
            syncAnimation.stop();
        }
        initScheduler();
        initUIExtra();
        txtCountryCode.setText(config.countryCode());
    }

    private void initScheduler() {
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::loadData, 0, config.refreshIntervalInSeconds(), TimeUnit.SECONDS);
    }

    private void loadData() {
        DataProviderService dataProviderService = new DataProviderService();
        CovidData covidData = dataProviderService.getData(config.countryName());
        Platform.runLater(() -> inflate(covidData)); //moves changes to the GUI thread
    }

    private void inflate(CovidData covidData) {
        GlobalData globalData = covidData.globalData();
        CountryData countryData = covidData.countryData();
        countryList = covidData.countryList();
        System.out.println("Country List = " +countryList.size());
        txtGlobalReport.setText(getFormattedData(globalData.cases(), globalData.recovered(), globalData.deaths()));
        txtCountryReport.setText(getFormattedData(countryData.cases(), countryData.recovered(), countryData.deaths()));
        try {
            config = new ConfigService().getConfiguration();
            txtCountryCode.setText(config.countryCode());
        } catch (IOException e) {
            e.printStackTrace();
        }
        adjustWindow(txtCountryReport.getScene().getWindow());
        syncAnimation.stop();
    }

    private String getFormattedData(long totalCases, long recoveredCases, long totalDeaths) {
        return String.format("Cases: %-8d | Recovered: %-8d | Deaths: %-8d", totalCases, recoveredCases, totalDeaths);

    }

    private void adjustWindow(Window window) {
        window.sizeToScene();
    }

    private void initUIExtra() {
        btnRefresh.setOnMouseClicked(event -> {
            syncAnimation.setCycleCount(1000);
            syncAnimation.play();
            executorService.schedule(this::loadData, 0, TimeUnit.SECONDS);
        });
        initStyles();
    }

    private void initStyles() {
        btnRefresh.getStyleClass().add("refreshStyle");
        btnRefresh.hoverProperty().addListener((observable, oldValue, newValue) -> {
            if (btnRefresh.isHover()) {
                syncAnimation.setCycleCount(1);
                syncAnimation.play();
            }
        });
    }

    public void showChangeCountry(MouseEvent event) throws IOException {
        URL resource = getClass().getResource("../change_country.fxml");
        FXMLLoader loader = new FXMLLoader(resource);
        loader.setClassLoader(cachingClassLoader);
        BorderPane parent = loader.load();
        ChangeCountryController controller = loader.getController();
        controller.setCountryList(countryList);
        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.show();
        stage.setOnHiding(event1 -> {
            syncAnimation.setCycleCount(1000);
            syncAnimation.play();
            try {
                config = new ConfigService().getConfiguration(); //re-read the file with the updated country data
            } catch (IOException e) {
                e.printStackTrace();
            }
            executorService.schedule(this::loadData, 0, TimeUnit.SECONDS);
        });
    }
}
