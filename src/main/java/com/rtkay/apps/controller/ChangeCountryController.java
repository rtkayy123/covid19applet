package com.rtkay.apps.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jfoenix.controls.JFXComboBox;
import com.rtkay.apps.config.Config;
import com.rtkay.apps.data.model.CountryData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import static com.rtkay.apps.config.ConfigService.SETTINGS_FILE;

public class ChangeCountryController {
    @FXML
    private JFXComboBox<CountryData> cmbCountries;
    private List<CountryData> countryList;

    public void setCountryList(List<CountryData> countryList) {
        this.countryList = countryList;
        inflateComboBox();
    }

    private void inflateComboBox() {
        cmbCountries.setItems(FXCollections.observableArrayList(countryList));
        Callback<ListView<CountryData>, ListCell<CountryData>> factory = lv -> new ListCell<>() {
            @Override
            protected void updateItem(CountryData item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.country());
            }
        };
        cmbCountries.setCellFactory(factory);
        cmbCountries.setButtonCell(factory.call(null));

    }


    public void updateCountry(MouseEvent event) {
        Gson gson = new GsonBuilder().create();
        CountryData countryData = cmbCountries.getSelectionModel().getSelectedItem();
        String countryName = countryData.country();
        String countryCode = "";
      /*  If country name has more than one word
      split them up and use the first letter of each word
      to be the country code - get the two first characters only
        */
        if (countryName.contains(" ")) {
            for (String s : countryName.split(" ")) {
                countryCode += s.charAt(0);
            }
            countryCode = countryCode.substring(0, 2);
        } else {
            countryCode = countryName.substring(0, 2);
            countryCode = countryCode.toUpperCase();
        }
        Config config = new Config(300, countryName, countryCode);
        try (Writer writer = new FileWriter(SETTINGS_FILE, false)) {
            gson.toJson(config, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeDialog(event);
    }

    public void closeDialog(MouseEvent event) {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        s.close();
    }
}
