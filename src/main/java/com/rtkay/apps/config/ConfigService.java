package com.rtkay.apps.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

public class ConfigService {
    public static final File SETTINGS_FILE = new File("settings.json");
    private Gson gson = new GsonBuilder().create();

    public Config getConfiguration() throws IOException {
        if (!SETTINGS_FILE.exists()) {
            createSettingsFile();
        }
        return getConfigFromFile();

    }

    private Config getConfigFromFile() throws IOException {
        try(Reader reader = new FileReader(SETTINGS_FILE)){
            return  gson.fromJson(reader,Config.class);
        }
    }

    private void createSettingsFile() {
        Config config = new Config(300,"South Africa","ZA");
        try(Writer writer = new FileWriter(SETTINGS_FILE,false)){
            gson.toJson(config,writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
