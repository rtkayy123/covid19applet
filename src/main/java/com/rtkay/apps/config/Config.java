package com.rtkay.apps.config;

public record Config (int refreshIntervalInSeconds, String countryName, String countryCode){
}
